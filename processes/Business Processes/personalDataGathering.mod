[Ivy]
1624CA41CC6F1D83 3.20 #module
>Proto >Proto Collection #zClass
pg0 personalDataGathering Big #zClass
pg0 B #cInfo
pg0 #process
pg0 @TextInP .resExport .resExport #zField
pg0 @TextInP .type .type #zField
pg0 @TextInP .processKind .processKind #zField
pg0 @AnnotationInP-0n ai ai #zField
pg0 @MessageFlowInP-0n messageIn messageIn #zField
pg0 @MessageFlowOutP-0n messageOut messageOut #zField
pg0 @TextInP .xml .xml #zField
pg0 @TextInP .responsibility .responsibility #zField
pg0 @StartRequest f0 '' #zField
pg0 @RichDialog f3 '' #zField
pg0 @PushWFArc f4 '' #zField
pg0 @StartRequest f5 '' #zField
pg0 @EndTask f6 '' #zField
pg0 @RichDialog f8 '' #zField
pg0 @PushWFArc f9 '' #zField
pg0 @PushWFArc f7 '' #zField
pg0 @PushWFArc f2 '' #zField
pg0 @EndTask f1 '' #zField
>Proto pg0 pg0 personalDataGathering #zField
pg0 f0 outLink start.ivp #txt
pg0 f0 type vn.axonactive.feedmo.personalDataGatheringData #txt
pg0 f0 inParamDecl '<> param;' #txt
pg0 f0 actionDecl 'vn.axonactive.feedmo.personalDataGatheringData out;
' #txt
pg0 f0 guid 1624CA41CCCABC9F #txt
pg0 f0 requestEnabled true #txt
pg0 f0 triggerEnabled false #txt
pg0 f0 callSignature start() #txt
pg0 f0 persist false #txt
pg0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
pg0 f0 caseData businessCase.attach=true #txt
pg0 f0 showInStartList 1 #txt
pg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startGathering.ivp</name>
        <nameStyle>18,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f0 @C|.responsibility Everybody #txt
pg0 f0 81 49 30 30 -48 17 #rect
pg0 f0 @|StartRequestIcon #fIcon
pg0 f3 targetWindow NEW #txt
pg0 f3 targetDisplay TOP #txt
pg0 f3 richDialogId vn.axonactive.feedmo.personalData #txt
pg0 f3 startMethod start() #txt
pg0 f3 type vn.axonactive.feedmo.personalDataGatheringData #txt
pg0 f3 requestActionDecl '<> param;' #txt
pg0 f3 responseActionDecl 'vn.axonactive.feedmo.personalDataGatheringData out;
' #txt
pg0 f3 responseMappingAction 'out=in;
out.name=result.nameParam;
out.team=result.teamParam;
' #txt
pg0 f3 isAsynch false #txt
pg0 f3 isInnerRd false #txt
pg0 f3 userContext '* ' #txt
pg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>personalData</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f3 168 42 112 44 -37 -8 #rect
pg0 f3 @|RichDialogIcon #fIcon
pg0 f4 expr out #txt
pg0 f4 111 64 168 64 #arcP
pg0 f5 outLink startVeritfyGathering.ivp #txt
pg0 f5 type vn.axonactive.feedmo.personalDataGatheringData #txt
pg0 f5 inParamDecl '<java.lang.String name,java.lang.String team> param;' #txt
pg0 f5 actionDecl 'vn.axonactive.feedmo.personalDataGatheringData out;
' #txt
pg0 f5 guid 1624CB4659415D5D #txt
pg0 f5 requestEnabled true #txt
pg0 f5 triggerEnabled false #txt
pg0 f5 callSignature startVeritfyGathering(String,String) #txt
pg0 f5 persist false #txt
pg0 f5 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
pg0 f5 caseData businessCase.attach=true #txt
pg0 f5 showInStartList 1 #txt
pg0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startVeritfyGathering.ivp</name>
        <nameStyle>25,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f5 @C|.responsibility Everybody #txt
pg0 f5 81 145 30 30 -64 17 #rect
pg0 f5 @|StartRequestIcon #fIcon
pg0 f6 type vn.axonactive.feedmo.personalDataGatheringData #txt
pg0 f6 337 145 30 30 0 15 #rect
pg0 f6 @|EndIcon #fIcon
pg0 f8 targetWindow NEW #txt
pg0 f8 targetDisplay TOP #txt
pg0 f8 richDialogId vn.axonactive.feedmo.vertifyPersonalData #txt
pg0 f8 startMethod start(vn.axonactive.feedmo.personalDataGatheringData) #txt
pg0 f8 type vn.axonactive.feedmo.personalDataGatheringData #txt
pg0 f8 requestActionDecl '<vn.axonactive.feedmo.personalDataGatheringData personalDataGatheringData> param;' #txt
pg0 f8 responseActionDecl 'vn.axonactive.feedmo.personalDataGatheringData out;
' #txt
pg0 f8 responseMappingAction 'out=in;
' #txt
pg0 f8 isAsynch false #txt
pg0 f8 isInnerRd false #txt
pg0 f8 userContext '* ' #txt
pg0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>vertifyGathering</name>
        <nameStyle>16,7
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f8 168 138 112 44 -42 -8 #rect
pg0 f8 @|RichDialogIcon #fIcon
pg0 f9 expr out #txt
pg0 f9 111 160 168 160 #arcP
pg0 f7 expr out #txt
pg0 f7 280 160 337 160 #arcP
pg0 f2 expr out #txt
pg0 f2 280 64 337 64 #arcP
pg0 f1 type vn.axonactive.feedmo.personalDataGatheringData #txt
pg0 f1 337 49 30 30 0 15 #rect
pg0 f1 @|EndIcon #fIcon
>Proto pg0 .type vn.axonactive.feedmo.personalDataGatheringData #txt
>Proto pg0 .processKind NORMAL #txt
>Proto pg0 0 0 32 24 18 0 #rect
>Proto pg0 @|BIcon #fIcon
pg0 f0 mainOut f4 tail #connect
pg0 f4 head f3 mainIn #connect
pg0 f3 mainOut f2 tail #connect
pg0 f2 head f1 mainIn #connect
pg0 f5 mainOut f9 tail #connect
pg0 f9 head f8 mainIn #connect
pg0 f8 mainOut f7 tail #connect
pg0 f7 head f6 mainIn #connect
