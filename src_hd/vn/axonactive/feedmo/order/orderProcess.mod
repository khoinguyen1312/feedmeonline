[Ivy]
1624C9D56CFF54AF 3.20 #module
>Proto >Proto Collection #zClass
os0 orderProcess Big #zClass
os0 RD #cInfo
os0 #process
os0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
os0 @TextInP .rdData2UIAction .rdData2UIAction #zField
os0 @TextInP .resExport .resExport #zField
os0 @TextInP .type .type #zField
os0 @TextInP .processKind .processKind #zField
os0 @AnnotationInP-0n ai ai #zField
os0 @MessageFlowInP-0n messageIn messageIn #zField
os0 @MessageFlowOutP-0n messageOut messageOut #zField
os0 @TextInP .xml .xml #zField
os0 @TextInP .responsibility .responsibility #zField
os0 @RichDialogInitStart f0 '' #zField
os0 @RichDialogProcessEnd f1 '' #zField
os0 @PushWFArc f2 '' #zField
os0 @RichDialogProcessStart f3 '' #zField
os0 @RichDialogEnd f4 '' #zField
os0 @PushWFArc f5 '' #zField
>Proto os0 os0 orderProcess #zField
os0 f0 guid 1624C9D56F540D73 #txt
os0 f0 type vn.axonactive.feedmo.order.orderData #txt
os0 f0 method start(vn.axonactive.feedmo.startData) #txt
os0 f0 disableUIEvents true #txt
os0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<vn.axonactive.feedmo.startData startData> param = methodEvent.getInputArguments();
' #txt
os0 f0 inParameterMapAction 'out.startData=param.startData;
' #txt
os0 f0 outParameterDecl '<> result;
' #txt
os0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(startData)</name>
    </language>
</elementInfo>
' #txt
os0 f0 83 51 26 26 -41 15 #rect
os0 f0 @|RichDialogInitStartIcon #fIcon
os0 f1 type vn.axonactive.feedmo.order.orderData #txt
os0 f1 211 51 26 26 0 12 #rect
os0 f1 @|RichDialogProcessEndIcon #fIcon
os0 f2 expr out #txt
os0 f2 109 64 211 64 #arcP
os0 f3 guid 1624C9D571488ED6 #txt
os0 f3 type vn.axonactive.feedmo.order.orderData #txt
os0 f3 actionDecl 'vn.axonactive.feedmo.order.orderData out;
' #txt
os0 f3 actionTable 'out=in;
' #txt
os0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
os0 f3 83 147 26 26 -15 12 #rect
os0 f3 @|RichDialogProcessStartIcon #fIcon
os0 f4 type vn.axonactive.feedmo.order.orderData #txt
os0 f4 guid 1624C9D5714C6375 #txt
os0 f4 211 147 26 26 0 12 #rect
os0 f4 @|RichDialogEndIcon #fIcon
os0 f5 expr out #txt
os0 f5 109 160 211 160 #arcP
>Proto os0 .type vn.axonactive.feedmo.order.orderData #txt
>Proto os0 .processKind HTML_DIALOG #txt
>Proto os0 -8 -8 16 16 16 26 #rect
>Proto os0 '' #fIcon
os0 f0 mainOut f2 tail #connect
os0 f2 head f1 mainIn #connect
os0 f3 mainOut f5 tail #connect
os0 f5 head f4 mainIn #connect
