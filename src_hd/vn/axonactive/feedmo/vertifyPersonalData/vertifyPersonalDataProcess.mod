[Ivy]
1624CB4E4D7A232B 3.20 #module
>Proto >Proto Collection #zClass
vs0 vertifyPersonalDataProcess Big #zClass
vs0 RD #cInfo
vs0 #process
vs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
vs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
vs0 @TextInP .resExport .resExport #zField
vs0 @TextInP .type .type #zField
vs0 @TextInP .processKind .processKind #zField
vs0 @AnnotationInP-0n ai ai #zField
vs0 @MessageFlowInP-0n messageIn messageIn #zField
vs0 @MessageFlowOutP-0n messageOut messageOut #zField
vs0 @TextInP .xml .xml #zField
vs0 @TextInP .responsibility .responsibility #zField
vs0 @RichDialogInitStart f0 '' #zField
vs0 @RichDialogProcessEnd f1 '' #zField
vs0 @PushWFArc f2 '' #zField
vs0 @RichDialogProcessStart f3 '' #zField
vs0 @RichDialogEnd f4 '' #zField
vs0 @PushWFArc f5 '' #zField
>Proto vs0 vs0 vertifyPersonalDataProcess #zField
vs0 f0 guid 1624CB4E4EC554F0 #txt
vs0 f0 type vn.axonactive.feedmo.vertifyPersonalData.vertifyPersonalDataData #txt
vs0 f0 method start(vn.axonactive.feedmo.personalDataGatheringData) #txt
vs0 f0 disableUIEvents true #txt
vs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<vn.axonactive.feedmo.personalDataGatheringData personalDataGatheringData> param = methodEvent.getInputArguments();
' #txt
vs0 f0 inParameterMapAction 'out.personalDataGatheringData=param.personalDataGatheringData;
' #txt
vs0 f0 outParameterDecl '<> result;
' #txt
vs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(personalDataGatheringData)</name>
    </language>
</elementInfo>
' #txt
vs0 f0 83 51 26 26 -93 15 #rect
vs0 f0 @|RichDialogInitStartIcon #fIcon
vs0 f1 type vn.axonactive.feedmo.vertifyPersonalData.vertifyPersonalDataData #txt
vs0 f1 211 51 26 26 0 12 #rect
vs0 f1 @|RichDialogProcessEndIcon #fIcon
vs0 f2 expr out #txt
vs0 f2 109 64 211 64 #arcP
vs0 f3 guid 1624CB4E4FAFA4EC #txt
vs0 f3 type vn.axonactive.feedmo.vertifyPersonalData.vertifyPersonalDataData #txt
vs0 f3 actionDecl 'vn.axonactive.feedmo.vertifyPersonalData.vertifyPersonalDataData out;
' #txt
vs0 f3 actionTable 'out=in;
' #txt
vs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
vs0 f3 83 147 26 26 -15 12 #rect
vs0 f3 @|RichDialogProcessStartIcon #fIcon
vs0 f4 type vn.axonactive.feedmo.vertifyPersonalData.vertifyPersonalDataData #txt
vs0 f4 guid 1624CB4E4FABA47C #txt
vs0 f4 211 147 26 26 0 12 #rect
vs0 f4 @|RichDialogEndIcon #fIcon
vs0 f5 expr out #txt
vs0 f5 109 160 211 160 #arcP
>Proto vs0 .type vn.axonactive.feedmo.vertifyPersonalData.vertifyPersonalDataData #txt
>Proto vs0 .processKind HTML_DIALOG #txt
>Proto vs0 -8 -8 16 16 16 26 #rect
>Proto vs0 '' #fIcon
vs0 f0 mainOut f2 tail #connect
vs0 f2 head f1 mainIn #connect
vs0 f3 mainOut f5 tail #connect
vs0 f5 head f4 mainIn #connect
